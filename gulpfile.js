/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2020-03-13 14:59:19
 * @LastEditors: qinyonglian
 * @LastEditTime: 2020-03-14 09:55:58
 */
const gulp = require('gulp');// 打包工具
const del = require('del');// 删除文件
const runSequence = require('gulp-run-sequence');// 任务执行顺序
require('./config-pc/package/gulp/gulp-build');
// 主入口
gulp.task('build', (cb) => {
  runSequence('clean', 'build-ext', 'build-resourse', 'build-rev', cb);
});

// 删除build,dist
gulp.task('clean', () => del([`${__dirname}/dist`]));
