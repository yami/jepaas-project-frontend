
const glob = require('glob');
const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const crypto = require('crypto');
const copyDir = require('copy-dir');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const config = require('../../config.js');

let entryDir = config.entryDir || 'src/pc/vue/modules';// 入口目录
const htmlTplDir = 'src/pc/tpl';// html模板目录
// 去掉最后的/
if (!entryDir.split('/').pop()) {
  entryDir = entryDir.substring(0, entryDir.length - 1);
}
const proFolder = 'pro';// 默认输出目录
const output = config.output || proFolder;// 输出目录
const rootFolder = '../../../';
module.exports = {
  config,
  dev: process.env.NODE_ENV === 'development', // 开发
  dist: 'static/vue',
  consoleInfo(text) {
    console.log('');
    console.log('--------------------------------------');
    console.log(chalk.green(text));
    console.log('--------------------------------------');
    console.log('');
  },
  /**
   * 复制公共资源
   */
  copyComment(entrys) {
    /* const commentAsset = path.resolve(__dirname, rootFolder, 'dist/_assets');
     if (fs.existsSync(commentAsset)) {
      let count = 0;
      entrys.forEach((entry) => {
        copyDir(commentAsset, path.resolve(__dirname, rootFolder, `dist/${entry}/assets`), () => {
          count++;
          if (entrys.length == count) {
            this.deleteFolder(commentAsset);
            console.log('公共资源复制成功！');
            this.hashMap();
          }
        });
      });
    } else {
      this.hashMap();
    } */
    this.hashMap();

    const files = glob.sync('dist/**/*.*.js');
    files.forEach((file) => {
      const dir = path.resolve(__dirname, rootFolder, file);
      let folder = file.split('/');
      folder.shift(); // 删除dist
      folder.pop(); // 删除文件名
      folder = [''].concat(folder).concat(['assets', '']);// 组建替换目录
      if (!this.dev && config.cdn) {
        folder.splice(0, 1, config.cdn);
      }
      let content = fs.readFileSync(dir, 'utf8');
      content = content.replace(/"\.\/assets\//g, `"${folder.join('/')}`);
      fs.writeFileSync(dir, content, 'utf8');
    });
    console.log('插件js资源引用路径替换完成！');
  },
  removeEntry(entrys) {
    entrys.forEach((entry) => {
      const dir = path.resolve(__dirname, rootFolder, `dist/${entry}`);
      if (fs.existsSync(dir)) {
        this.deleteFolder(dir);
        console.log(chalk.green(`${dir} 删除成功！`));
      }
    });
  },
  buildPublicPath(url) {
    return url;
    let paths = url.split('/');
    if (paths[0] == '_assets') {
      paths.splice(0, 1, '.', 'assets');
    } else {
      paths = paths.splice(paths.indexOf('assets'), paths.length);
      paths.unshift('.');
    }
    url = paths.join('/');
    return url;
  },
  /**
     * 资源文件目录
     * @param {资源路径} url
     * @param {资源类型} type
     */
  buildAssetsUrl(url, type, entrys) {
    return `${this.dist}/assets/${type}/[name].[hash:10].[ext]`;
    let name = `assets/${type}/[name]-[hash:10].[ext]`;// 默认资源目录
    for (const entry in entrys) {
      const entryFolder = path.dirname(path.resolve(__dirname, '../../../', entrys[entry]));
      // 如果是同一个入口，放入对应的入口目录
      if (url.startsWith(entryFolder)) {
        name = `${entry}/${name}`;
        break;
      }
    }
    // 如果没有，放入公共目录
    if (name.startsWith('assets')) {
      name = `_${name}`;
    }
    return name;
  },
  /**
     * 自动构建入口文件
     */
  buildEntry() {
    const entrys = {};
    // 动态读取入口
    const files = glob.sync(`./${entryDir}/*/index.js`);
    const _entrys = [];
    files.forEach((f) => {
      const folders = f.split('/');
      _entrys.push(folders[folders.length - 2]);
    });
    // 配置入口
    (config.entry || _entrys).forEach((_entry) => {
      let entry = _entry;
      let file = `${entryDir}/${entry}/index.js`;
      if (typeof _entry == 'object') {
        file = `${_entry.folder}/index.js`;
        // eslint-disable-next-line prefer-destructuring
        entry = _entry.entry;
      }
      if (fs.existsSync(path.resolve(__dirname, rootFolder, file))) {
        const configFile = path.resolve(__dirname, rootFolder, file.replace('index.js', 'config.json'));
        let _output = output;
        if (fs.existsSync(configFile)) {
          const cfg = JSON.parse(fs.readFileSync(configFile, 'utf-8'));
          if (cfg.output && cfg.output.length > 0) {
            _output = cfg.output == entry ? '' : cfg.output.toLowerCase();
          }
        }
        // entrys[`${_output}/${entry}`] = `./${file}`;
        entrys[`${entry}`] = `./${file}`;
        this.entryPath[entry] = `${_output}/${entry}`;
      }
    });
    return entrys;
  },
  entryPath: {},
  /**
     * 构建extjs视图文件
     */
  buildViews(entrys) {
    if (config.view == false) return;

    // 写入的文件夹
    const writeDir = path.join(__dirname, rootFolder, 'dist');
    // 模板
    const tpl = fs.readFileSync(path.join(__dirname, '../', 'tpl/View.js'), 'utf8');
    entrys.forEach((_entry) => {
      const entry = this.entryPath[_entry];
      const classArray = entry.split('/');
      const xtype = classArray.join('.').toLowerCase();
      classArray[0] = classArray[0].toUpperCase();// 包名大写
      classArray.push('View');
      const className = classArray.join('.');
      // 替换模版文件中的内容
      const info = tpl.replace(/{class}/g, className).replace(/{xtype}/g, xtype).replace(/{entry}/g, entry);
      let viewPath = path.join(`${writeDir}/${entry}`);
      if (!fs.existsSync(viewPath)) {
        // fs.mkdirsSync(viewPath, { recursive: true });
        this.mkdirsSync(viewPath, { recursive: true });
      }
      if (this.dev) {
        viewPath += '/View.js';
      } else {
        const hash = crypto.createHash('md5').update(info).digest('hex').substr(0, 10);// MD5版本
        viewPath += `/View.${hash}.js`;
      }
      fs.writeFileSync(viewPath, info, {
        encoding: 'UTF-8',
      });
    });
  },
  // 递归创建目录 同步方法
  mkdirsSync(dirname) {
    if (fs.existsSync(dirname)) {
      return true;
    }
    if (this.mkdirsSync(path.dirname(dirname))) {
      fs.mkdirSync(dirname);
      return true;
    }
  },
  /**
     * 驼峰转-
     * @param {String} name
     */
  toLine(name) {
    return name.replace(/([A-Z])/g, '-$1').toLowerCase();
  },
  /**
   * 删除目录
   * @param {String} path 路径
   */
  deleteFolder(folder) {
    const me = this; let
      files = [];
    if (fs.existsSync(folder)) {
      files = fs.readdirSync(folder);
      files.forEach((file) => {
        const curPath = path.join(folder, file);
        if (fs.statSync(curPath).isDirectory()) { // recurse
          me.deleteFolder(curPath);
        } else { // delete file
          fs.unlinkSync(curPath);
        }
      });
      fs.rmdirSync(folder);
    }
  },
  /**
   * 构建html模板
   * @param {*} entrys 入口文件
   * @returns {Array}
   */
  buildHtmlTpl(entrys) {
    const files = glob.sync(`${htmlTplDir}/*/config.json`);
    const entryKeys = Object.keys(entrys);
    const tpls = [];
    files.forEach((file) => {
      // 读取配置文件
      const cfg = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../../../', file), 'utf-8'));
      // 模板配置信息
      const htmlCfg = {
        template: path.resolve(__dirname, '../../../', file.replace('/config.json', '/index.html')),
        inject: false,
        filename: cfg.output,
        chunks: [],
        minify: true, // 不压缩
        cdn: this.dev ? '' : config.cdn || '',
        server: config.proxyServerUrl || '',
      };

      // 需要的配置信息
      if (cfg.configInfo) {
        htmlCfg[cfg.configInfo] = config[cfg.configInfo];
      }
      // 依赖的模块
      cfg.entry.forEach((key) => {
        entryKeys.forEach((entry) => {
          if (entry == key) {
            htmlCfg.chunks.push(entry);
          }
        });
      });
      if (htmlCfg.chunks.length > 0) {
        tpls.push(new HtmlWebpackPlugin(htmlCfg));
      }
    });
    return tpls;
  },
  hashMap() {
    if (config.hashMap == false) return;

    const hash = {};
    const dist = 'dist/';
    const files = glob.sync(`${dist}**/*`);
    files.forEach((file) => {
      file = file.replace(dist, '');
      const paths = file.split('/');
      const hashName = paths.pop();
      if (hashName.includes('.') && !hashName.endsWith('.html')) {
        const name = hashName.replace(/[-.](\d|[a-z]){10}/, '');
        if (!file.startsWith('static/vue/') || ['index.js', 'index.css'].includes(name)) {
          hash[paths.concat([name]).join('/')] = paths.concat([hashName]).join('/');
        }
      }
    });
    // 创建目录
    const dir = path.resolve(__dirname, rootFolder, 'dist', 'static/js');
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    // 内容
    let content = '';
    if (Object.keys(hash).length > 0) {
      content += `window.__online_info__ && (window.__online_info__.hash_map=Object.assign({}, window.__online_info__.hash_map,${JSON.stringify(hash)}));`;
    }
    // 增加复写样式
    const css = 'static/project/override.css';
    const cssFile = path.resolve(__dirname, '../../resourse/project', config.project, css);
    if (fs.existsSync(cssFile)) {
      const _dt = new Date().getTime();
      content += `document.write('<link rel="stylesheet" href="'+(window.__online_info__ && window.__online_info__.cdn || '')+'/${css}?${_dt}" />');`;
    }
    // 生成映射文件
    fs.writeFileSync(path.resolve(dir, 'hash-map-open.js'), content, 'utf-8');
    console.log(chalk.gray('<<<<<< hashMap.js 构建完成 '));
  },
  _hashMap() {
    if (config.hashMap == false) return;
    const hash = {}; const
      distFolder = ['pro', 'plugin', 'static/vue'];
    const folders = [{ base: 'dist/', exclude: ['login.html', '_assets/'] }, {
      base: 'src/pc/ext/', dev: true, exclude: [], folder: `${proFolder}/`,
    }];
    folders.forEach((item) => {
      if (!item.dev || item.dev && this.dev) {
        const files = glob.sync(`${item.base}/**/*`);
        files.forEach((file) => {
          file = file.replace(item.base, item.folder || '');
          const paths = file.split('/');
          const hashName = paths.pop();
          if (hashName.includes('.')) {
            let flag = true;
            item.exclude.forEach((ex) => {
              if (file.startsWith(ex)) {
                flag = false;
                return false;
              }
            });
            if (flag) {
              const name = hashName.replace(/[-.](\d|[a-z]){10}/, '');
              let folder = paths[0];
              folder = distFolder.includes(folder) ? folder : proFolder;
              hash[folder] = hash[folder] || {};
              hash[folder][paths.concat([name]).join('/')] = paths.concat([hashName]).join('/');
            }
          }
        });
      }
    });
    Object.keys(hash).forEach((folder) => {
      const maps = hash[folder];
      const dir = path.resolve(__dirname, rootFolder, 'dist', folder);
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
      let content = `window.__online_info__ && (window.__online_info__.hash_map=Object.assign({}, window.__online_info__.hash_map,${JSON.stringify(maps)}));`;
      // 增加复写样式
      if (folder == 'pro') {
        const css = 'static/project/override.css';
        const cssFile = path.resolve(__dirname, '../../resourse/project', config.project, css);
        if (fs.existsSync(cssFile)) {
          const _dt = new Date().getTime();
          content += `document.write('<link rel="stylesheet" href="'+(window.__online_info__ && window.__online_info__.cdn || '')+'/${css}?${_dt}" />');`;
        }
      }
      fs.writeFileSync(path.resolve(dir, 'hash-map.js'), content, 'utf-8');
    });
    console.log(chalk.gray('<<<<<< hashMap.js 构建完成 '));
  },
};
