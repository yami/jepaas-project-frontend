

const path = require('path');
const merge = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { VueLoaderPlugin } = require('vue-loader');

module.exports = (util) => {
  const root = path.resolve(__dirname, '../../../');// 根目录
  const output = path.resolve(__dirname, '../../../', 'dist');// 输出目录
  const srcRoot = path.resolve(__dirname, '../../../', 'src/pc/vue');// 源码目录
  const { dev, dist } = util;

  let cfg = {};
  if (dev) {
    cfg = require('./webpack.dev.js');
  } else {
    cfg = require('./webpack.prod.js');
  }

  const entry = util.buildEntry();
  const entrys = entry;// Object.keys(entry);
  // 构建html页
  const plugins = util.buildHtmlTpl(entrys);
  let publicPath = util.config.cdn || '';
  if (util.dev) {
    publicPath = '';
  } else if (!publicPath.endsWith('/')) {
    publicPath += '/';
  }
  return merge({
    entry,
    context: root,
    devtool: 'none',
    externals: { // 不打包的第三方库
      vue: 'Vue',
      'vue-router': 'VueRouter',
      vuex: 'Vuex',
      'element-ui': 'ELEMENT',
    },
    stats: {
    },
    output: {
      publicPath,
      path: output,
      filename: dev ? `${dist}/[name]/index.js` : `${dist}/[name]/index.[contenthash:10].js`,
      chunkFilename: dev ? `${dist}/[name].js` : `${dist}/[name].[contenthash:10].js`,
      libraryTarget: 'umd',
      umdNamedDefine: true,
    },
    resolve: {
      extensions: ['.js', '.vue', '.json', '.css', '.less'],
      alias: {
        '@': root,
        '@_src': srcRoot,
      },
    },
    module: {
      rules: [
        {
          test: /\.(css|postcss|scss)(\?.*)?$/,
          include: srcRoot,
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            'postcss-loader',
            'sass-loader',
          ],
        },
        {
          test: /\.(css|postcss|less)(\?.*)?$/,
          include: srcRoot,
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            'postcss-loader',
            { loader: 'less-loader', options: { javascriptEnabled: true } },
          ],
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          include: srcRoot,
        },
        {
          test: /\.js$/,
          loader: 'babel-loader',
          include: srcRoot,
        },
        {
          test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
          loader: 'url-loader',
          include: srcRoot,
          options: {
            limit: 10000,
            name(url) {
              return util.buildAssetsUrl(url, 'images', entrys);
            },
          },
        },
        {
          test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
          loader: 'url-loader',
          include: srcRoot,
          options: {
            limit: 10000,
            name(url) {
              return util.buildAssetsUrl(url, 'media', entrys);
            },
          },
        },
        {
          test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
          loader: 'url-loader',
          include: srcRoot,
          options: {
            limit: 10000,
            name(url) {
              return util.buildAssetsUrl(url, 'fonts', entrys);
            },
          },
        },
      ],
    },
    plugins: [
      new VueLoaderPlugin(),
      new MiniCssExtractPlugin({ // 抽取css
        filename: dev ? `${dist}/[name]/index.css` : `${dist}/[name]/index.[contenthash:10].css`,
        chunkFilename: dev ? `${dist}/[name].css` : `${dist}/[name].[contenthash:10].css`,
      }),
      ...plugins,
    ],
  }, cfg);
};
