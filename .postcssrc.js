/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2019-11-22 15:00:16
 * @LastEditors: qinyonglian
 * @LastEditTime: 2020-03-13 15:06:35
 */
if (process.env.NODE_ITEM == 'APP') {
  module.exports = {
    "plugins": {
      "postcss-import": {},
      "postcss-url": {},
      "autoprefixer": {},
      'postcss-pxtorem': {
        rootValue: 75,
        unitPrecision: 5,
        propList: ['*'],
        selectorBlackList: [],
        replace: true,
        mediaQuery: false,
        minPixelValue: 15,
      },
    }
  }
}
if (process.env.NODE_ITEM == 'PC') {
  module.exports = {
    "plugins": {
      "postcss-import": {},
      "postcss-url": {},
      "autoprefixer": {},
    }
  }
}
