/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2020-03-15 13:24:41
 * @LastEditors: qinyonglian
 * @LastEditTime: 2020-03-15 13:34:37
 */
// 服务器地址
import { HOST_BASIC } from '../../../constants/config';

// 冒泡列表
export const POST_FIND_BUBBLE_MSG = `${HOST_BASIC}/je/portal/homePortal/findBubbleMsg`;
