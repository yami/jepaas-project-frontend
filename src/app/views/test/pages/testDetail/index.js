/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2020-03-15 14:23:30
 * @LastEditors: qinyonglian
 * @LastEditTime: 2020-03-15 14:25:38
 */
import Vue from 'vue';
import App from './index.vue';


Vue.config.productionTip = false;
Vue.config.devtools = true;

new Vue({
  render: h => h(App),
}).$mount('#app');
