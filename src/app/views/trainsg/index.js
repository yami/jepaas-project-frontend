/*
 * @Descripttion:
 * @Author: 张明尧
 * @Date: 2021-02-02 14:37:21
 * @LastEditTime: 2021-02-07 10:14:34
 */
import router from './router';
import config from './config.json';
import install from '../../util/install';

install(router, config);
