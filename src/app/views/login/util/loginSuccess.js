/*
 * @Descripttion:
 * @Author: 张明尧
 * @Date: 2021-02-02 14:37:21
 * @LastEditTime: 2021-02-02 15:41:55
 */
/**
 * @Author : ZiQin Zhai
 * @Date : 2019/11/9 18:59
 * @Version : 1.0
 * @Last Modifined by : ZiQin Zhai
 * @Last Modifined time : 2019/11/9 18:59
 * @Description
 * */
import { getBeforeUrl, removeBeforeUrl } from './auth';

/**
 * 登录成功后执行的逻辑
 */
export function loginSuccess(vue) {
  if (window.plus) {
    plus.webview.getLaunchWebview()
      .evalJS('APP.start.openHome()');
    return false;
  }
  // 跳转到上一级目录
  let beforeUrl = getBeforeUrl();
  if (!beforeUrl) {
    beforeUrl = '/home';
    removeBeforeUrl();
  }
  vue.$router.push(beforeUrl);
  setTimeout(() => {
    vue.$router.push('/home');
  }, 500);
  JE.initSysData();
}
