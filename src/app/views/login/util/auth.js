/**
 * @Author : ZiQin Zhai
 * @Date : 2019/6/14 10:46
 * @Version : 1.0
 * @Last Modifined by : ZiQin Zhai
 * @Last Modifined time : 2019/6/14 10:46
 * @Description 授权相关
 * */
const TOKENKEY = 'authorization';
const CURRENT_USER = '_current_user_';

export function getToken() {
  return JE.getLSItem(TOKENKEY);
}

export function setToken(val) {
  JE.setLSItem(TOKENKEY, val);
}

export function removeToken() {
  localStorage.removeItem(TOKENKEY);
  JE.setLSItem(CURRENT_USER, null);
}

const beforeLoginUrl = 'beforeLoginUrl';

// 获取之前页面的信息
export function getBeforeUrl() {
  return sessionStorage.getItem(beforeLoginUrl);
}

export function setBeforeUrl(url) {
  return sessionStorage.setItem(beforeLoginUrl, url);
}

export function removeBeforeUrl() {
  sessionStorage.removeItem(beforeLoginUrl);
}
